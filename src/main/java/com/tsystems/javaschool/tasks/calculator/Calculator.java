package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
        if (wrongExpressionCheck(statement)) {
            return null;
        }
        else {
            //решение
                MatchParser parser = new MatchParser();
                double res = parser.Parse(statement);
                String result = Double.toString(Math.round(res * 10000.0) / 10000.0);
                while (result.charAt(result.length()-1) == '0' || result.charAt(result.length()-1) == '.' ) {
                    if (result.charAt(result.length()-1) == '0') {
                        result = result.substring(0, result.length()-1);
                    }
                    if (result.charAt(result.length()-1) == '.') {
                        result = result.substring(0, result.length()-1);
                        break;
                    }
                }
                return result;
            }
        }
        catch (Exception e) {
            return null;
        }
    }

    private boolean wrongExpressionCheck(String statement) {
        boolean expressionIsWrong;
        Pattern pattern = Pattern.compile("([.+/\\-*]{2,})|([,a-zA-Z]+)|([\\s]+)");
        expressionIsWrong = pattern.matcher(statement).find();

        int roundBracketCount = 0;
        char[] charArrayStatement = statement.toCharArray();
        for (int i = 0; i < charArrayStatement.length; i++) {
            if (charArrayStatement[i] == '(') {
                roundBracketCount++;
            }
            if (charArrayStatement[i] == ')') {
                roundBracketCount--;
            }
            if (roundBracketCount < 0) {
                expressionIsWrong = true;
                break;
            }
        }
        if (roundBracketCount > 0) {expressionIsWrong = true;}
        return expressionIsWrong;
    }

    class Result {
        public double acc;
        public String rest;

        public Result(double v, String r)
        {
            this.acc = v;
            this.rest = r;
        }
    }

    class MatchParser
    {
        public MatchParser() {}

        public double Parse(String s) throws Exception
        {
            Result result = PlusMinus(s);
            if (!result.rest.isEmpty()) {
                throw new Exception();
            }
            return result.acc;
        }

        private Result PlusMinus(String s) throws Exception
        {
            Result current = MulDiv(s);
            double acc = current.acc;

            while (current.rest.length() > 0) {
                if (!(current.rest.charAt(0) == '+' || current.rest.charAt(0) == '-')) break;

                char sign = current.rest.charAt(0);
                String next = current.rest.substring(1);

                current = MulDiv(next);
                if (sign == '+') {
                    acc += current.acc;
                } else {
                    acc -= current.acc;
                }
            }
            return new Result(acc, current.rest);
        }

        private Result Bracket(String s) throws Exception
        {
            char zeroChar = s.charAt(0);
            if (zeroChar == '(') {
                Result r = PlusMinus(s.substring(1));
                if (!r.rest.isEmpty() && r.rest.charAt(0) == ')') {
                    r.rest = r.rest.substring(1);
                }
                return r;
            }
            return Num(s);
        }

        private Result MulDiv(String s) throws Exception
        {
            Result current = Bracket(s);

            double acc = current.acc;
            while (true) {
                if (current.rest.length() == 0) {
                    return current;
                }
                char sign = current.rest.charAt(0);
                if ((sign != '*' && sign != '/')) return current;

                String next = current.rest.substring(1);
                Result right = Bracket(next);

                if (sign == '*') {
                    acc *= right.acc;
                } else {
                    if (right.acc != 0) {
                        acc /= right.acc;
                    }
                    else {
                        throw new Exception();
                    }
                }
                current = new Result(acc, right.rest);
            }
        }

        private Result Num(String s) throws Exception
        {
            int i = 0;
            boolean negative = false;
            if( s.charAt(0) == '-' ){
                negative = true;
                s = s.substring( 1 );
            }
            while (i < s.length() && (Character.isDigit(s.charAt(i)) || s.charAt(i) == '.')) {
                i++;
            }
            if( i == 0 ){
                throw new Exception();
            }

            double dPart = Double.parseDouble(s.substring(0, i));
            if( negative ) dPart = -dPart;
            String restPart = s.substring(i);

            return new Result(dPart, restPart);
        }
    }



}
