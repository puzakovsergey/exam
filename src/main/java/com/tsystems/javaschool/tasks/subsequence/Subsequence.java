package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null | y == null) {
            throw new IllegalArgumentException();
        }
        else {
            int indexInY = 0;
            for (Object objX : x) {
                boolean elementIsFound = false;
                for (int i = indexInY; i < y.size(); i++) {
                    if (objX.equals(y.get(i))) {
                        indexInY = i + 1;
                        elementIsFound = true;
                        break;
                    }
                }
                if (!elementIsFound) {
                    return false;
                }
            }
            return true;
        }
    }
}
