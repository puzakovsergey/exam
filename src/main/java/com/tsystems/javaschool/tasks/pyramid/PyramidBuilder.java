package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            int rowCount = rowCountCalculation(inputNumbers.size());
            if (rowCount > 0) {
                return makePyramid(inputNumbers, rowCount);
            } else {
                throw new CannotBuildPyramidException();
            }
        }
        catch (Exception e) {
            throw new CannotBuildPyramidException();
        }

    }

    public int rowCountCalculation (Integer listSize) {
        int d = 1+4*1*2*listSize;
        double rowCount = ((-1) + Math.sqrt(d))/2;
        if (rowCount%1 == 0) {
            return (int) rowCount;
        }
        else return -1;
    }

    public int[][] createEmty2DArray(int rowCount) {
        int[][] array = new int[rowCount][2 * rowCount - 1];
        for (int i = 0; i < rowCount; i++) {
            for (int k = 0; k < 2 * rowCount - 1; k++) {
                array[i][k] = 0;
            }
        }
        return array;
    }

    public int[][] makePyramid(List<Integer> nums, int rowCount) {
        int[][] pyramid = createEmty2DArray(rowCount);
        Collections.sort(nums);
        Iterator<Integer> iterator = nums.iterator();
        for (int i = 0; i < rowCount; i++) {
                int startPosition = rowCount - i - 1;
                int counter = 0;
                while (counter <= i) {
                    pyramid[i][startPosition] = iterator.next();
                    startPosition = startPosition + 2;
                    counter++;
                }
            }
        return pyramid;
    }


}
